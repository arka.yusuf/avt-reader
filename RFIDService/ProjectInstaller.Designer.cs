﻿using System.Configuration;
namespace RFIDService
{
    partial class ProjectInstaller
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            /*this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;*/
            // 
            // serviceInstaller1
            // 
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            if (config.AppSettings.Settings["post_id"] != null)
            {
                string post_id = config.AppSettings.Settings["post_id"].Value;
                this.serviceInstaller1.ServiceName = "RFID Reader Service " + post_id;
                this.serviceInstaller1.DisplayName = "RFID Reader Service " + post_id;
                this.serviceInstaller1.Description = "This service connect and listen into RFID reader devices for Post ID : " + post_id;
            }
            //this.serviceInstaller1.Description = "This service connect and listen into RFID reader devices";
            //this.serviceInstaller1.ServiceName = "RFID Reader "+post_id;
            //this.serviceInstaller1.DisplayName = "RFID Reader "+post_id;
            this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1,
            this.serviceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;
    }
}