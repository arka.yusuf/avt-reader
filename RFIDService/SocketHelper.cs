﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace RFIDService
{
    class SocketHelper
    {
        public SocketPermission permission;
        public IPAddress ipAddr;
        public IPEndPoint ipEndPoint;
        public string socket_ip;
        public int socket_port;
        Socket sender;
        LogWriter log = new LogWriter();
        DatabaseHelper db = new DatabaseHelper();
        public SocketHelper(string PROCESS_ID)
        {
            var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            socket_port = Convert.ToInt32(config.AppSettings.Settings["socket_port"].Value);
            socket_ip = config.AppSettings.Settings["socket_ip"].Value;
            
            //("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Connecting to socket : IP : "+socket_ip+", PORT : "+socket_port);
            try{
                log.Write("Connecting to socket ip : " + socket_ip + ", port : " + socket_port);
                permission = new SocketPermission(
                NetworkAccess.Connect, 
                TransportType.Tcp,
                "",
                SocketPermission.AllPorts
                );
                
                permission.Demand();
                ipAddr = IPAddress.Parse(socket_ip);
                ipEndPoint = new IPEndPoint(ipAddr, socket_port);
                sender = new Socket(
                        ipAddr.AddressFamily,
                        SocketType.Stream,
                        ProtocolType.Tcp
                        );
                sender.NoDelay = false;
                sender.Connect(ipEndPoint);
            }
            catch(Exception ex)
            {
                //("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Socket Connection Exception : "+ex.Message);
                log.Write("socket connection exception : "+ex.Message);
            }
            
        }

        public void sendToSocket(string message, string PROCESS_ID)
        {
            log.Write("SOCKET IP : " + socket_ip);
            //("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Sending body no through socket");
            try
            {
                //log.Write("Send data to socket, message data : "+message);
                byte[] msg = Encoding.ASCII.GetBytes(message);
                sender.Send(msg);
                //("113", PROCESS_ID, "2", "RFID Reader Service", "INF", "Body no has been sent through socket");
            }
            catch(Exception ex)
            {
                //("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Send Socket Data Exception : "+ex.Message);
                log.Write("Send data to socket exception : " + ex.Message);
            }
        }

        public void closeSocket(string PROCESS_ID)
        {
            try
            {
                //("113", PROCESS_ID, "2", "RFID Reader Service", "INF", "Closing socket from ip : " + socket_ip + ", port : " + socket_port);
                //log.Write("Closing socket from ip : " + socket_ip+", port : "+socket_port);
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
                //("113", PROCESS_ID, "2", "RFID Reader Service", "INF", "Process finished successfuly");
            }
            catch(Exception ex)
            {
                log.Write("Closing socket exception : " + ex.Message);
                ////("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Closing socket exception : " + ex.Message);
                ////("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Process finished with errors");
            }
            
        }
    }
}
