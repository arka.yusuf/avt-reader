﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDService
{
    class RFIDData
    {
        public string POST_ID { get; set; }
        public string READER_IP { get; set; }
        public string STATUS { get; set; }
        public string POST_SEQ { get; set; }
    }
}
