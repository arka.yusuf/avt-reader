﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RFIDService
{
    class BodyData
    {
        public string BODY_NO { get; set; }
        public string POST_SEQ { get; set; }
        public string SEQ_NO { get; set; }
        public string VINNO { get; set; }
    }
}
