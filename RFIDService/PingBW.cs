﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;

namespace RFIDService
{
    class PingBW
    {
        private Ping myPing = new Ping();
        private string mess = "";
        private BackgroundWorker pingping;
        private string COMMAND;
        private string IP_ADDRESS;
        private string POST_ID;
        private DatabaseHelper db = new DatabaseHelper();
        private LogWriter log = new LogWriter();
        private bool is_connected = false;
        private string PROCESS_ID;
        private string POST_SEQ;
        public PingBW(string command, string ip_address, string post_id, string post_seq)
        {
            
            pingping = new BackgroundWorker();
            pingping.WorkerReportsProgress = true;
            pingping.DoWork += new System.ComponentModel.DoWorkEventHandler(pingping_DoWork);
            pingping.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(pingping_ProgressChanged);
            pingping.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(pingping_RunWorkerCompleted);
            this.COMMAND = command;
            this.IP_ADDRESS = ip_address;
            this.POST_ID = post_id;
            this.POST_SEQ = post_seq;
            Object p = new { command = this.COMMAND, ip_address = this.IP_ADDRESS, reply = "", post_id = this.POST_ID, post_seq = this.POST_SEQ };
            pingping.RunWorkerAsync(p);
        }

        private void pingping_DoWork(object sender, DoWorkEventArgs workEventArgs)
        {
           
            try
            {
                
                //log.Write("pingping_DoWork : started");
                Object p = workEventArgs.Argument;
                string ip_address = (String)p.GetType().GetProperty("ip_address").GetValue(p, null);
                
                string post_id = (String)p.GetType().GetProperty("post_id").GetValue(p, null);
                string command = (String)p.GetType().GetProperty("command").GetValue(p, null);
                string post_seq = (String)p.GetType().GetProperty("post_seq").GetValue(p, null);
                //PROCESS_ID = ////("113", "", "1", "RFID Reader Service", "INF", "Checking RFID device connectivity on IP Address : "+ip_address);
                //log.Write(PROCESS_ID);
                PingReply reply = myPing.Send(ip_address, 1000);
                Object rep = new { command = command, ip_address = ip_address, reply = reply.Status.ToString(), post_id = post_id, post_seq=post_seq};
                workEventArgs.Result = rep;
            }
            catch (Exception ex)
            {
//log.Write("pingping_DoWork : " + ex.Message);
                workEventArgs.Result = ex.Message;
            }

        }
        private void pingping_ProgressChanged(object sender, ProgressChangedEventArgs progressEventArgs)
        {
            Console.WriteLine(progressEventArgs);
            //log.Write("pingping_ProgressChanged : " + progressEventArgs);
        }
        private void pingping_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs connectEventArgs)
        {
           // log.Write("pingping_RunWorkerCompleted");
            try
            {
                Object p = connectEventArgs.Result;
                string ip_address = (String)p.GetType().GetProperty("ip_address").GetValue(p, null);
                string post_id = (String)p.GetType().GetProperty("post_id").GetValue(p, null);
                string command = (String)p.GetType().GetProperty("command").GetValue(p, null);
                string result = (String)p.GetType().GetProperty("reply").GetValue(p, null);
                string post_seq = (String)p.GetType().GetProperty("post_seq").GetValue(p, null);
                //log.Write("pingping_RunWorkerCompleted result : (" +ip_address+" : "+ result+")");
                if (result == "Success")
                {
                    if (mess != "Network Connected")
                    {
                        mess = "Network Connected : " + ip_address;
                        //log.Write("pingping_RunWorkerCompleted : updating status as connected");
                        //////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Device : "+ip_address+" is available");
                        //////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Updating device availability status into TB_M_RFID_H");
                        db.updateConnection(ip_address, "1");
                        //Console.WriteLine(mess);
                        if (!is_connected)
                        {

                            Thread.Sleep(5000);
                            //////("113", PROCESS_ID, "1", "RFID Reader Service", "INF", "Connecting to RFID reader device");
                            
                            rfidBW w = new rfidBW("Connect", ip_address, post_id, PROCESS_ID, post_seq);
                        }
                        is_connected = true;
                    }
                    /*
                    if (!m_IsConnected)
                    {
                        connectBackgroundWorker.RunWorkerAsync("Connect");
                    }
                    */
                }
                else
                {
                    if (mess != "Network Disconnected")
                    {
                        //log.Write("pingping_RunWorkerCompleted : updating status as disconnected");
                        mess = "Network Disconnected : " + ip_address;
                        //////("113", PROCESS_ID, "3", "RFID Reader Service", "ERR", "Device : " + ip_address + " is unavailable");
                        db.updateConnection(ip_address, "0");
                        //Console.WriteLine(mess);
                    }
                }
                Thread.Sleep(5000);
                pingping.RunWorkerAsync(p);
            }
            catch(Exception ec)
            {
                //log.Write(ec.Message);
            }
            
        }
    }
}
